import time
import sys
from config import *

pygame.init()
clock = pygame.time.Clock()
background_music_location = "../images/background_music.mp3"
background_music = pygame.mixer.music.load(background_music_location)
pygame.mixer.music.play(loops=-1)
font = pygame.font.Font(pygame.font.get_default_font(), font_size)

# function to display the score

def score_display():
    total_points = point_calculate()
    global point_player_1, point_player_2
    global point_player_1_so_far, point_player_2_so_far
    if flag == 0:
        score_player1 = font.render(
            "POINTS:" + str(total_points + point_player_1_so_far),
            True, color_font)
        screen.blit(score_player1, (text_pos_x, text_pos_y))
    else:
        score_player1 = font.render(
            "PLAYER 1 POINTS:" + str(point_player_1 + point_player_1_so_far),
            True, color_font)
        score_player2 = font.render(
            " PLAYER 2 POINTS:" + str(total_points + point_player_2_so_far),
            True, color_font)
        screen.blit(score_player1, (text_pos_x, text_pos_y))
        screen.blit(score_player2, (text_pos_x, text_pos_y + 550))


# displaying the start and the endpoints
# function to display the start and the end points of the player
def destination_display():
    destination_1 = "END"
    destination_2 = "START"
    obj1 = font.render(destination_1, True, color_font)
    destination_player_text.append(obj1)
    obj2 = font.render(destination_2, True, color_font)
    destination_player_text.append(obj2)
    for index in range(0, 2):
        if flag == 1:
            screen.blit(destination_player_text[1 - index],
                        (destination_player_pos[index][0],
                         destination_player_pos[index][1]))
        else:
            screen.blit(destination_player_text[index],
                        (destination_player_pos[index][0],
                         destination_player_pos[index][1]))


# initializing the background
background_image = pygame.image.load(background_image)
background_image = pygame.transform.scale(background_image, (800, 600))

background_image_opening = pygame.image.load(background_image_opening)
background_image_opening = pygame.transform.scale(background_image_opening,
                                                  (800, 600))


# destinations

def background_init():
    global background_image
    screen.blit(background_image, (0, 0))


# player display
player_icon = pygame.image.load(player_icon)
player_icon = pygame.transform.scale(player_icon, (48, 48))
player_cur_x = player_init_x
player_cur_y = player_init_y

# enemy list. make n number of lists for n attributes
# where ith attribute is of ith monster
# 5 moving enemies each river having one

Enemy_icon = []
# makes List of the enemies required
def Enemy_constructor():
    no_of_enemies = len(Enemy_pos)
    enemy_dyn_loc = "../images/ghost.png"
    for i in range(0, no_of_enemies):
        obj = pygame.image.load(enemy_dyn_loc)
        obj = pygame.transform.scale(obj, (48, 48))
        Enemy_icon.append(obj)




# moving the enemies horizontally
def Enemy_pos_change():
    if flag != 2:
        no_of_enemies = len(Enemy_pos)
        for index in range(0, no_of_enemies):
            Enemy_pos[index][0] = (Enemy_pos[index][0] - Enemy_speed + 800) \
                                  % 800
        for index in range(0, no_of_enemies):
            screen.blit(Enemy_icon[index],
                        (Enemy_pos[index][0], Enemy_pos[index][1]))


# making static enemies which would kill the player in specific range

Enemy_static_icon = []
total_enemies = len(Enemy_static_pos)

#setting images to all the static enemies
def Enemy_static_constructor():
    global total_enemies
    # enemy_stat_loc = configParser.get("enemy", "enemy_stat_loc")[1:-1]
    enemy_stat_loc = "../images/tombstone.png"
    for i in range(0, total_enemies):
        obj = pygame.image.load(enemy_stat_loc)
        obj = pygame.transform.scale(obj, (48, 48))
        Enemy_static_icon.append(obj)

#spawning static enemies based on predefined coords
def Enemy_static_spawner():
    global total_enemies
    for index in range(0, total_enemies):
        screen.blit(Enemy_static_icon[index], (Enemy_static_pos[index][0],
                                               Enemy_static_pos[index][1]))


# point system for player

#calculating points based on coords
def point_calculate():
    # calculating for static enemies
    global player_cur_y
    point_moving = 0
    point_static = 0
    for enemy in Enemy_static_pos:
        if player_cur_y < enemy[1] and flag == 0:
            point_static += 1
        elif player_cur_y > enemy[1] and flag == 1:
            point_static += 1
    for enemy in Enemy_pos:
        if player_cur_y < enemy[1] and flag == 0:
            point_moving += 1
        if player_cur_y > enemy[1] and flag == 1:
            point_moving += 1
    total_points = (point_moving * 10) + (point_static * 5)
    return total_points


# function to update the player by moving it

def player_pos_change(player_x, player_y):
    # print(player_x, player_y)
    player_x = player_x % 800
    # moving from left part of the screen to the right part
    if player_y <= 10:
        player_y = 10
    if player_y >= 570:  # preventing the player from leaving the screen
        player_y = 570
    elif player_y <= 50 and player_x > 360 and player_x < 450 and flag == 0:
        player_win()
    elif player_y > 540 and player_x < 470 and player_x > 390 and flag == 1:
        player_win()
    # elif player_x
    screen.blit(player_icon, (player_x, player_y))
    return player_x, player_y





# function to move the player
def player_move():
    print("function call")
    global player_cur_x, player_cur_y
    if (player_input_fix[0]):
        player_cur_y += player_change_value
    elif (player_input_fix[1]):
        player_cur_y += -player_change_value
    if (player_input_fix[2]):
        player_cur_x += player_change_value
    elif (player_input_fix[3]):
        player_cur_x += -player_change_value

#taking input from the player based wasd for player 2 and arrows for player 1
def player_input(cur_event, player_change_x, player_change_y):
    global screen_on
    global player_input_fix
    if cur_event.type == pygame.QUIT:
        screen_on = False
    if flag != 2:
        if cur_event.type == pygame.KEYDOWN:
            if (cur_event.key == pygame.K_LEFT and flag == 0) or \
                    (cur_event.key == pygame.K_a and flag == 1) or \
                    player_input_fix[3]:
                player_input_fix[3] = True
            if (cur_event.key == pygame.K_RIGHT and flag == 0) or \
                    (cur_event.key == pygame.K_d and flag == 1) or \
                    player_input_fix[2]:
                player_input_fix[2] = True
            if (cur_event.key == pygame.K_UP and flag == 0) or \
                    (cur_event.key == pygame.K_w and flag == 1) or \
                    player_input_fix[1]:
                player_input_fix[1] = True
            if (cur_event.key == pygame.K_DOWN and flag == 0) or \
                    (cur_event.key == pygame.K_s and flag == 1) or \
                    player_input_fix[0]:
                player_input_fix[0] = True
        if cur_event.type == pygame.KEYUP:
            if cur_event.key == pygame.K_LEFT or cur_event.key == pygame.K_a:
                player_input_fix[3] = False
            if cur_event.key == pygame.K_RIGHT or cur_event.key == pygame.K_d:
                player_input_fix[2] = False
            if cur_event.key == pygame.K_UP or cur_event.key == pygame.K_w:
                player_input_fix[1] = False
            if cur_event.key == pygame.K_DOWN or cur_event.key == pygame.K_s:
                player_input_fix[0] = False
            if cur_event.key == pygame.K_LEFT or \
                    cur_event.key == pygame.K_RIGHT:
                player_change_x = 0
            if cur_event.key == pygame.K_UP or \
                    cur_event.key == pygame.K_DOWN:
                player_change_y = 0
    return player_change_x, player_change_y




#deciding who wins based on time and points
def player_win():
    global flag
    global player_cur_x, player_cur_y
    global player_init_x, player_init_y
    if flag == 0:
        global point_player_1_so_far
        point_player_1_so_far += point_calculate()
        global time_player1
        time_player1 += time.time() - time_total
        player_cur_x = player2_init_x
        player_cur_y = player2_init_y
        flag = 1
    else:
        global point_player_2_so_far
        point_player_2_so_far += point_calculate()
        global time_player2
        time_player2 += time.time() - time_total
        player_cur_x = player_init_x
        player_cur_y = player_init_y
        flag = 0
        global Enemy_speed
        Enemy_speed *= 2

#using coords to check collision
def player_collision_detection(player_cur_x, player_cur_y, Enemy_pos):
    for enemy in Enemy_pos:
        player_enemy_gap_x = abs(enemy[0] - player_cur_x)
        player_enemy_gap_y = abs(enemy[1] - player_cur_y)
        # print(player_enemy_gap)
        if player_enemy_gap_x < 25 and player_enemy_gap_y < 25:
            player_out_reset()
            # print("player out")
    for enemy in Enemy_static_pos:
        player_enemy_gap_x = abs(enemy[0] - player_cur_x)
        player_enemy_gap_y = abs(enemy[1] - player_cur_y)
        # print(player_enemy_gap)
        if player_enemy_gap_x < 25 and player_enemy_gap_y < 25:
            player_out_reset()



# once the player dies we have to reset his position back to the
# initial position and then display that the player died
def player_out_reset():
    global player_cur_x
    global player_cur_y
    global flag
    if flag == 0:
        global point_player_1
        point_player_1 = point_calculate()
        global time_player1
        time_player1 = time.time() - time_total
        player_cur_x = player2_init_x
        player_cur_y = player2_init_y
        flag = 1
    else:
        global time_player2
        time_player2 = time.time() - time_player2
        global point_player_2
        point_player_2 = point_calculate()
        game_over()
        flag = 2



final_font = pygame.font.Font(pygame.font.get_default_font(), 64)
score_win_1 = final_font.render(player_1_win_text, True, color_font)
score_win_2 = final_font.render(player_2_win_text, True, color_font)
score_draw = font.render(player_draw, True, color_font)



def game_reset():
    global destination_player_text
    global destination_player_pos
    global player_cur_x
    global player_cur_y
    global Enemy_icon
    global Enemy_speed
    global Enemy_pos
    global player_input_fix
    global point_player_1_so_far
    global point_player_2_so_far
    global time_player1
    global time_total
    global time_player1
    global time_player2
    global point_player_1
    global point_player_2
    global time_track
    global point_static
    global point_moving
    global flag
    destination_player_text = []
    destination_player_pos = [[400, 0], [400, 560]]
    player_cur_x = player_init_x
    player_cur_y = player_init_y
    Enemy_icon = []
    Enemy_speed = 1
    Enemy_pos = [[random.randint(0, 136), 50],
                 [random.randint(0, 136), 160],
                 [random.randint(0, 136), 270],
                 [random.randint(0, 136), 380],
                 [random.randint(0, 136), 490],
                 [random.randint(136, 272), 50],
                 [random.randint(136, 272), 160],
                 [random.randint(136, 272), 270],
                 [random.randint(136, 272), 380],
                 [random.randint(136, 272), 490],
                 [random.randint(272, 408), 50],
                 [random.randint(272, 408), 160],
                 [random.randint(272, 408), 270],
                 [random.randint(272, 408), 380],
                 [random.randint(272, 408), 490],
                 [random.randint(408, 544), 50],
                 [random.randint(408, 544), 160],
                 [random.randint(408, 544), 270],
                 [random.randint(408, 544), 380],
                 [random.randint(408, 544), 490],
                 [random.randint(544, 680), 50],
                 [random.randint(544, 680), 160],
                 [random.randint(544, 680), 270],
                 [random.randint(544, 680), 380],
                 [random.randint(544, 680), 490],
                 ]
    player_input_fix = [False, False, False, False]
    point_player_1_so_far = 0
    point_player_2_so_far = 0
    time_player1 = 0
    time_total = 0
    time_player1 = 0
    time_player2 = 0
    point_player_1 = 0
    point_player_2 = 0
    Enemy_constructor()
    Enemy_static_constructor()
    time_track = time.time()
    point_static = 0
    point_moving = 0
    flag = 0




def game_over():
    global point_player_1, point_player_2
    global point_player_1_so_far, point_player_2_so_far
    global score_win_1, score_win_2, score_draw
    player1 = point_player_1_so_far + point_player_1
    player2 = point_player_2_so_far + point_player_2
    if player1 > player2:
        score_win = score_win_1
    elif player1 < player2:
        score_win = score_win_2
    else:
        if time_player1 > time_player2:
            score_win = score_win_2
        elif time_player1 < time_player2:
            score_win = score_win_1
        else:
            score_win = score_draw
    screen.blit(score_win, (150, 270))


screen = pygame.display.set_mode((800, 600))
opening_screen = pygame.display.set_mode((800, 600))
screen_on = True

# Changing the title and the icon of the window

pygame.display.set_caption("Test_game")

# loop to run it always


Enemy_constructor()
Enemy_static_constructor()
time_track = time.time()
while opening_screen_on:
    opening_screen.blit(background_image_opening, (0, 0))
    for cur_event in pygame.event.get():
        print("for loop")
        if cur_event.type == pygame.KEYDOWN:
            if cur_event.key == pygame.K_SPACE:
                opening_screen_on = False
            elif cur_event.key == pygame.K_ESCAPE:
                opening_screen_on = False
                screen_on = False
    pygame.display.update()

while screen_on:
    clock.tick(fps)
    if flag == 2:
        pygame.time.delay(1000)
        game_reset()
    else:
        point_calculate()
        background_init()
        Enemy_static_spawner()
        destination_display()
        Enemy_pos_change()
        player_collision_detection(player_cur_x, player_cur_y, Enemy_pos)
        player_change_x = 0
        player_change_y = 0
        player_move()
        for cur_event in pygame.event.get():
            player_change_x, player_change_y = \
                player_input(cur_event, player_change_x, player_change_y)
        player_cur_x += player_change_x
        player_cur_y += player_change_y
        player_cur_x, player_cur_y = \
            player_pos_change(player_cur_x, player_cur_y)
        score_display()
        pygame.display.update()
        print(player_cur_x, player_cur_y)

pygame.quit()
quit()
sys.exit()
