import random
import pygame
destination_2 = "START"


background_image = "../images/background_edit.png"
background_image_opening = "../images/starting_screen.png"

fps = 60
color_font = (247, 247, 247)
font_size = 32
text_pos_y = 5
text_pos_x = 5
player_icon = "../images/icon.png"
player_init_x = 420
player_init_y = 570
player_change_value = 3
player2_init_x = 420
player2_init_y = 10
point_player_1_so_far = 0
point_player_2_so_far = 0
time_player1 = 0
time_total = 0
time_player2 = 0
point_player_1 = 0
point_player_2 = 0
player_1_win_text = "PLAYER 1 WINS"
player_2_win_text = "PLAYER 2 WINS"
destination_player_text = []
destination_player_pos = [[400, 0], [400, 560]]
destination_1 = "END"
destination_2 = "START"
background_music_location = "../images/background_music.mp3"

Enemy_speed = 1
enemy_dyn_loc = "../images/ghost.png"
enemy_stat_loc = "../images/tombstone.png"

Enemy_static_pos = [
    [300, 0], [500, 0],
    [10, 105], [140, 105], [240, 105], [350, 105], [480, 105],
    [580, 105], [680, 105], [780, 105],

    [30, 210], [160, 210], [250, 210], [350, 210], [450, 210],
    [560, 210], [670, 210], [760, 210],

    [10, 320], [130, 320], [300, 320], [420, 320], [530, 320],
    [650, 320], [760, 320],

    [60, 430], [250, 430], [150, 430], [420, 430], [590, 430], [720, 430],

    [350, 550], [500, 550]
]

Enemy_pos = [[random.randint(0, 136), 50],
             [random.randint(0, 136), 160],
             [random.randint(0, 136), 270],
             [random.randint(0, 136), 380],
             [random.randint(0, 136), 490],
             [random.randint(136, 272), 50],
             [random.randint(136, 272), 160],
             [random.randint(136, 272), 270],
             [random.randint(136, 272), 380],
             [random.randint(136, 272), 490],
             [random.randint(272, 408), 50],
             [random.randint(272, 408), 160],
             [random.randint(272, 408), 270],
             [random.randint(272, 408), 380],
             [random.randint(272, 408), 490],
             [random.randint(408, 544), 50],
             [random.randint(408, 544), 160],
             [random.randint(408, 544), 270],
             [random.randint(408, 544), 380],
             [random.randint(408, 544), 490],
             [random.randint(544, 680), 50],
             [random.randint(544, 680), 160],
             [random.randint(544, 680), 270],
             [random.randint(544, 680), 380],
             [random.randint(544, 680), 490]
             ]
player_input_fix = [False, False, False, False]
death_player = [False, False]
player_1_win_text = "PLAYER 1 WINS"
player_2_win_text = "PLAYER 2 WINS"
player_draw = "ITS A DRAW"
point_static = 0
point_moving = 0
flag = 0
start_flag = True
opening_screen_on = True