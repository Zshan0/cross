CROSSING GAME

This game was made possible with the help of pygame and icons and other images taken from flaticon.com

This game is about crossing an area without hitting the obstacles, to do this the players can move using arrow keys and the 2nd player can move using wasd keys

The points are based on the number of enemies the player crosses and it is different for static and moving enemies

for the moving enemies, the points are double of the points of static enemies. 

As one player either completes or dies in the game, the control is shifted to the other player

game terminates when player 2 dies and score is decided according to the points gained over the rounds by both the players

By any chance if the points are same then the tie is broken by the time duration, lesser time duration player wins the game. if that too is equal then it is a draw

As you progress over the levels, the speed of the moving obstacles increase but the number of enemies do not increase.
